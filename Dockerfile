FROM php:7.3-cli-alpine

WORKDIR /var/www/html

RUN apk add --update --no-cache postgresql-dev; \
		docker-php-ext-install \
		pdo_mysql \
        pdo_pgsql \
        pgsql

ADD --chown=www-data:www-data . .
